import MD5 from "react-native-md5";
import HttpService from './HttpService';
const config = require ('../config/config');

function baseURL() {
    return config.backendUri + "/users/register";
}

export function register(email, pass) {
    return new Promise((resolve, reject) => {
        HttpService.post(baseURL(), {
            email: email,
            password: MD5.hex_md5(pass),
        }, function (data) {
            resolve(data);
        }, function (textStatus) {
            reject(textStatus);
        });
    });
}
