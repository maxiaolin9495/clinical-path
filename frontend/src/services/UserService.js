import HttpService from './HttpService';
const config = require ('../config/config');

function baseURL() {
    return config.backendUri;
}

export function uploadMessage(message, email, firstName) {
    return new Promise((resolve, reject) => {
        HttpService.post(baseURL()+'/contact/message', {
            email: email,
            firstName: firstName,
            message: message
        }, function (data){
            resolve(data);
        } ,function (textStatus) {
            reject(textStatus);
        });
    });
}
