import React from 'react';
import {Button, TextField, CardTitle} from 'react-md';
import { toast } from 'react-toastify';

class RegisterTab extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
            passwordConfirm: '',
        }
    }

    render() {
        return (
            <div className="md-grid" id="registrationTable"
                 style={{
                     width: '30%',
                     marginTop: '10%',
                     background: 'white',
                     minWidth: '300px'
                 }}>
                <CardTitle title="Register" id='RegisterTitle' style={{
                    marginLeft: 'auto',
                    marginRight: 'auto'
                }}/>
                <form className="md-grid" onSubmit={this.handleSubmit}>
                    <TextField
                        id="floating-center-email"
                        label="Email"
                        required
                        lineDirection="center"
                        placeholder="Please input your emailAddress"
                        onChange={value => this.handleChange('email', value)}
                        className="md-cell md-cell--bottom"
                        style={{
                            marginTop: '-20px',
                            width: '100%'
                        }}
                    />
                    <TextField
                        id="floating-password"
                        label="Please Input your password"
                        required
                        type="password"
                        onChange={value => this.handleChange('password', value)}
                        className="md-cell md-cell--bottom"
                        style={{
                            width: '100%'
                        }}
                    />
                    <TextField
                        id="floating-password-confirm"
                        label="Confirm your password"
                        required
                        type="password"
                        onChange={value => this.handleChange('passwordConfirm', value)}
                        className="md-cell md-cell--bottom"
                        style={{
                            width: '100%'
                        }}
                    />
                    <Button id="submit" type="submit" flat primary swapTheming style={{
                        marginTop: '10px',
                        marginLeft: 'auto',
                        marginRight: 'auto'
                    }}>Register</Button>
                </form>
            </div>
        )
    }

    isEmail = () => {
        if (this.state.email.search(/^\w+((-\w+)|(\.\w+))*@[A-Za-z0-9]+(([.\-])[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/) !== -1) {
            return true;
        } else {
            document.getElementById('floating-center-email').value = "Please input valid Email Address";
            document.getElementById('floating-center-email').focus();
            return false;
        }
    };
    verifyPassword = () => {
        if (this.state.password.length < 8 ){
            this.setState({
                password: '',
                passwordConfirm: ''
            });
            document.getElementById('floating-password').label = "Password length must larger than 8";
            document.getElementById('floating-password-confirm').value = "";
            document.getElementById('floating-password').value = "";
            document.getElementById('floating-password').focus();
            toast.error('Password length must longer than 8');
            return false;
        }


        if(this.state.password.search(/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/) === -1){
            document.getElementById('floating-password-confirm').value = "";
            document.getElementById('floating-password').value = "";
            document.getElementById('floating-password').focus();
            toast.error('The password must contain at least 1 lowercase and 1uppercase alphabetical and 1 numerical character');
            return false;
        }

        if ( this.state.password === this.state.passwordConfirm) {
            return true;
        } else {
            toast.error("Password does not match");
            this.setState({
                password: '',
                passwordConfirm: ''
            });
            document.getElementById('floating-password').label = "Passwords are not matching";
            document.getElementById('floating-password-confirm').value = "";
            document.getElementById('floating-password').value = "";
            document.getElementById('floating-password').focus();
            return false;
        }
    };
    handleChange = (key, val) => {
        this.setState({
            [key]: val
        })
    };

    handleSubmit = (event) => {
        if (event)
            event.preventDefault();
        if (!this.isEmail() || !this.verifyPassword()) {
            return;
        }
        let user = {
            email: this.state.email,
            password: this.state.password,
        };

        this.props.onSubmit(user);
    }


}

export default RegisterTab;
