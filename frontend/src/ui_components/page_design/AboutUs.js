import React from "react";
import {CardTitle, CardText} from "react-md";


export default function AboutUs () {
        return (<div className="md-grid" id="AboutUsTable"  style={{
            display: 'flex',
            width: '30%',
            margin: '0 auto',
            marginTop: '10%',
            background: 'white',
            minWidth: '300px'
        }}>
                <CardTitle title="About us" id='AboutUsTitle' style={{
                    marginLeft: 'auto',
                    marginRight: 'auto'
                }}/>
                <CardText>
                    <p>
                    It is an example frontend skeleton integrated login&registration
                    </p>
                </CardText>
        </div>)
}
