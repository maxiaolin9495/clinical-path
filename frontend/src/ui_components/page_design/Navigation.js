import React from 'react';
import {Route, Link, withRouter } from 'react-router-dom';
import {Avatar, FontIcon, ListItem, NavigationDrawer, Button, IconSeparator } from 'react-md';
import imgURL from '../../images/LogoIcon.png';
import './Navigation.css';
import {logout, isAuthenticated} from '../../services/LoginService';

const Item = ({label, children}) => (
    <IconSeparator style={{fontFamily: 'cursive', fontSize: '25px', fontWeight: 'bold'}} label={label} iconBefore
                   component="li" className="md-cell md-cell--12">
        {children}
    </IconSeparator>
);

const NavLink = ({label, to, exact, icon}) => (
    <Route path={to} exact={exact}>
        {({match}) => {
            let leftIcon;
            if (icon) {
                leftIcon = <FontIcon>{icon}</FontIcon>;
            }

            return (
                <ListItem
                    component={Link}
                    active={!!match}
                    to={to}
                    primaryText={label}
                    leftIcon={leftIcon}
                />
            );
        }}
    </Route>
);

const defaultNavItems = [
    {
        label: 'Home',
        to: '/',
        exact: true,
        icon: 'home',
    },
];

const logInNavItems = [
    {
        label: 'Home',
        to: '/',
        exact: true,
        icon: 'home',
    },
    {
        label: 'Contact us',
        to: '/contact-us',
        icon: 'send'
    }
];




class NavigationMenu extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            loggedIn : isAuthenticated()
        }
    }

    logout = () => {
        logout();
        this.setState({
            loggedIn: false
        }, () => {
            this.props.history.push('/');
        });
    }

    toolBarActions =(loggedIn)=>{
        return loggedIn ?
            <Button id="logoutButton" type="button" flat primary swapTheming
                    onClick={this.logout}>Log out</Button>
            :
            <div id="noneName">
                <Button type="button" id="loginButton" flat primary swapTheming
                        onClick={() => this.props.history.push('/login')}>Login</Button>
                <Button type="button" id="RegistrationButton" flat primary swapTheming
                        onClick={() => this.props.history.push('/register')}>Register</Button>
            </div>
    }

    render() {
        return (
            <NavigationDrawer
                desktopDrawerType={NavigationDrawer.DrawerTypes.TEMPORARY}
                className="NavigationMenuStyle"
                drawerTitle="Menu"
                toolbarActions={
                    this.toolBarActions(this.state.loggedIn)
                }
                navItems={
                    navItems(this.state.loggedIn)
                }
                toolbarTitle={

                    <Item label="FindMyTutor">
                        <Avatar src={imgURL}
                                role="presentation"
                                suffix="green-400"
                                onClick={() => this.props.history.push('/')}/>
                    </Item>
                }
            />
        );
    }
}

function navItems(loggedIn){
    return (loggedIn?
        logInNavItems :
        defaultNavItems).map(props => <NavLink {...props} key={props.to}/>)
}

export default withRouter(NavigationMenu)
