import React from 'react';
import AboutUs from '../ui_components/page_design/AboutUs';
import Background from "../images/Homepage.jpg";
import Navigation from "../ui_components/page_design/Navigation";

export class AboutUsView extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            data: [],
        };
    }

    render() {
        setTimeout(() => window.scrollTo(0, 0), 150);
        return (
            <div>
                <Navigation/>
                <section>
                    <img src={Background} alt={"A Background Picture"} className="bg"/>
                    <AboutUs/>
                </section>
            </div>
        );
    }
}
