import React from 'react';
import ContactUs from '../ui_components/page_design/ContactUs';
import Background from "../images/Homepage.jpg";
import {uploadMessage} from "../services/UserService";
import { toast } from 'react-toastify';
import Navigation from "../ui_components/page_design/Navigation";

export class ContactUsView extends React.Component {

    send = (contactForm) => {
        uploadMessage(contactForm.message, contactForm.email, contactForm.firstName)
            .then(() => {
                toast.success("Message successfully sent out");
                this.props.history.push('/');
            }).catch(e => {
            console.log(e);
            toast.error(e);
        });
    };

    constructor(props) {
        super(props);

        this.state = {
            loading: false,
            data: [],
        };
    }

    render() {
        setTimeout(() => window.scrollTo(0, 0), 150);
        return (
            <div>
                <Navigation/>
                <section>
                    <img src={Background} className="bg" alt={'Background'}/>
                    <ContactUs onSubmit={(contactForm) => this.send(contactForm)} error={this.state.error}/>
                </section>
            </div>
        );
    }
}
