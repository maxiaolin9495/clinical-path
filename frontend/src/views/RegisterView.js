import React from "react";
import Background from "../images/Homepage.jpg";
import RegisterTab from "../ui_components/page_design/Register";
import {register} from "../services/RegisterService";
import { toast } from 'react-toastify';
import Navigation from "../ui_components/page_design/Navigation";

export class RegisterView extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};

    }

    register = (user) => {
        register(user.email, user.password)
            .then(() => {
                toast.success('Registration succeeded');
                this.props.history.push('/');
            }).catch((e) => {
            toast.error('Registration failed, please check your input');
            this.setState({
                error: e
            });
        })
    };

    render() {
        setTimeout(() => window.scrollTo(0, 0), 150);
        return (
            <div>
                <Navigation/>
                <section>
                    <img src={Background} alt={"A Background Picture"} className="bg"/>
                    <RegisterTab onSubmit={(user) => this.register(user)} error={this.state.error}/>
                </section>
            </div>

        )

    }
}
