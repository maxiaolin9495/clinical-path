import React from "react";
import Background from "../images/Homepage.jpg";
import LoginTab from "../ui_components/page_design/Login";
import {login} from "../services/LoginService";
import { toast } from 'react-toastify';
import Navigation from "../ui_components/page_design/Navigation";

export class LoginView extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    login = (user) => {
        login(user.email, user.password)
            .then(() => {
                toast.success('Login succeeded');
                this.props.history.push('/');

            }).catch((e) => {
            toast.error('Please input correct email and password');
            document.getElementById('floating-password').value = '';
            this.setState({
                error: e
            });
        });
    };

    render() {
        setTimeout(() => window.scrollTo(0, 0), 150);
        return (
            <div>
                <Navigation/>
                <section className="sectionBody">
                    <img src={Background} className="bg" alt={'Background'}/>
                    <LoginTab onSubmit={(user) => this.login(user)} error={this.state.error}/>
                </section>
            </div>
        )
    }
}
