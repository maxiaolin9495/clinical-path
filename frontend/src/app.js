import React from 'react';
import { HashRouter, Switch, Route } from 'react-router-dom';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { LoginView } from "./views/LoginView";
import { RegisterView } from "./views/RegisterView";
import { AboutUsView } from "./views/AboutUsView";
import { ContactUsView } from "./views/ContactUsView";

export default class App extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            title: 'React17 + webpack5',
            routes: [
                { component: LoginView, path: '/login', exact: true },
                { component: RegisterView, path: '/register', exact: true },
                { component: AboutUsView, path: '/', exact: true },
                { component: ContactUsView, path: '/contact-us', exact: true },
            ]
        };
    }

    componentDidMount() {
        document.title = this.state.title;
    }

    render() {
        return (
            <div>
                <HashRouter>
                    <Switch>
                        {this.state.routes.map((route, i) => (<Route key={i} {...route} />))}
                    </Switch>
                </HashRouter>
                <ToastContainer
                    position="bottom-right"
                    autoClose={5000}
                    hideProgressBar={false}
                    newestOnTop={false}
                    closeOnClick
                    rtl={false}
                    pauseOnFocusLoss
                    draggable
                    pauseOnHover
                />
            </div>
        );
    }
}
