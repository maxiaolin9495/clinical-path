package com.clinicalpath.dataObjects;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import static com.fasterxml.jackson.annotation.JsonProperty.Access.WRITE_ONLY;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Message {

    private String email;

    private String firstName;

    @JsonProperty(access = WRITE_ONLY)
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private String message;

    private MessageStatus status;

}
