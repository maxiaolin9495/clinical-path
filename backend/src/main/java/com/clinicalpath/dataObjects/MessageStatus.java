package com.clinicalpath.dataObjects;

public enum MessageStatus {
    created,
    inProgress,
    closed
}
