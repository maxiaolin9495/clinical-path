package com.clinicalpath.controllers;

import javax.annotation.Resource;

import com.clinicalpath.dataObjects.LoginResponse;
import com.clinicalpath.dataObjects.User;
import com.clinicalpath.repositories.UserDao;
import com.clinicalpath.security.TokenGenerationService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import static org.springframework.util.ObjectUtils.isEmpty;

@CrossOrigin(origins = {"http://localhost:8080"})
@Controller
@RequestMapping("/users")
@Api(value = "User", tags = {"Authorization & Users"})
public class UserController {

    private static final Logger log = LoggerFactory.getLogger(UserController.class);

    @Resource
    private UserDao userRepository;

    @Resource
    private TokenGenerationService tokenGenerationService;

    @ApiOperation("Register a new user account")
    @PostMapping(value = "/register", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<User> register(@RequestBody User user) {

        if ( isEmpty(user.getEmail())
                || isEmpty(user.getPassword())) {
            return new ResponseEntity("{}",HttpStatus.BAD_REQUEST);
        }
        log.info("received user registration request with username " + user.getEmail());

        if (userRepository.findUserByEmail(user.getEmail()) != null) {
            log.error("user with the same username has registered");
            return new ResponseEntity("{}", HttpStatus.BAD_REQUEST);
        }

        User createdUser = userRepository.save(user);
        log.info("user registered successfully");
        return new ResponseEntity<>(createdUser, HttpStatus.OK);
    }

    @ApiOperation("Login with an existing user account that is allowed to login")
    @PostMapping(value = "/login", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<LoginResponse> login(@RequestBody User user) {

        if ( isEmpty(user.getEmail())
                || isEmpty(user.getPassword())) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        log.info("received user login request with username " + user.getEmail());
        User foundUser = userRepository.findUserByEmail(user.getEmail());
        if (foundUser == null) {
            log.error("User not found");
            return new ResponseEntity("{}", HttpStatus.NOT_FOUND);
        }

        if (user.getPassword().equals(foundUser.getPassword())) {
            LoginResponse loginResponse;
            try {
                log.info("password matched");
                loginResponse = tokenGenerationService.generateToken(foundUser);
                log.info("token generated, user logged in successfully");
                return new ResponseEntity<>(loginResponse, HttpStatus.OK);
            } catch (Exception e) {
                log.error("Failed to generate token, caused by " + e.getMessage());
                return new ResponseEntity("{}", HttpStatus.INTERNAL_SERVER_ERROR);
            }

        }
        log.error("password does not match for user " + user.getEmail());
        return new ResponseEntity("{}", HttpStatus.UNAUTHORIZED);
    }

}
