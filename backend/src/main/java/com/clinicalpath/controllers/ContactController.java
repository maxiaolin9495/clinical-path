package com.clinicalpath.controllers;

import com.clinicalpath.dataObjects.Message;
import com.clinicalpath.dataObjects.MessageStatus;
import com.clinicalpath.dataObjects.User;
import com.clinicalpath.repositories.UserDao;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import static org.springframework.util.ObjectUtils.isEmpty;

@Controller
@RequestMapping("/contact")
@Api(value = "Contact", tags = {"Contact"})
public class ContactController {

    private static final Logger log = LoggerFactory.getLogger(ContactController.class);

    @Autowired
    private UserDao userRepository;

    @ApiOperation("User sends a feedback")
    @PostMapping(value = "/message", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity saveMessage(@RequestBody Message message) {
        if(isEmpty(message.getEmail())
                || isEmpty(message.getFirstName())
                || isEmpty(message.getMessage()) ) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        log.info("received user's feedback with email " + message.getEmail());

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User authenticationPrincipal = (User) authentication.getPrincipal();
        if(!authenticationPrincipal.getEmail().equals(message.getEmail())){
            log.error("the email address inside the request does not match the token's subject");
            return new ResponseEntity<>("{}", HttpStatus.FORBIDDEN);
        }

        User user = userRepository.findUserByEmail(message.getEmail());

        if(user == null) {
            log.error("can not find a relevant user, ignore the feedback");
            return new ResponseEntity<>("{}", HttpStatus.OK);
        }
        message.setStatus(MessageStatus.created);

        user.addMessage(message);

        User createdUser = userRepository.save(user);
        log.info("message saved successfully");
        return new ResponseEntity<>("{}", HttpStatus.OK);
    }

}
