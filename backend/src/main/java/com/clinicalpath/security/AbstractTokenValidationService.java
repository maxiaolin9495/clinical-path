package com.clinicalpath.security;

import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;

import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWSVerifier;
import com.nimbusds.jose.crypto.RSASSAVerifier;
import com.nimbusds.jwt.SignedJWT;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class AbstractTokenValidationService {

    @Value("${token.issuer}")
    private String VALID_ISSUER;

    @Value("${public_key}")
    private String BACKEND_PUBLIC_KEY;

    @Value("${client.id}")
    private String CLIEND_ID;

    private JWSVerifier verifier;

    private static final Logger log = LoggerFactory.getLogger(AbstractTokenValidationService.class);

    @PostConstruct
    private void init() throws NoSuchAlgorithmException, InvalidKeySpecException {
        try {
            byte[] decBackendPubKey = Base64.getDecoder().decode(BACKEND_PUBLIC_KEY);
            RSAPublicKey publicKey;
            publicKey = (RSAPublicKey) KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec((decBackendPubKey)));
            this.verifier = new RSASSAVerifier(publicKey);
            log.info("initialized jws verifier");
        } catch (InvalidKeySpecException e) {
            log.error("invalid public key");
            throw e;
        } catch (NoSuchAlgorithmException e) {
            log.error("Algorithm not found");
            throw e;
        }
    }


    public boolean validateToken(SignedJWT signedJWT) {
        try {
            return validateSignature(signedJWT) &&
                    validateIssuer(signedJWT.getJWTClaimsSet().getIssuer()) &&
                    validateExpiresAt(signedJWT.getJWTClaimsSet().getExpirationTime())
                    && validateAudience(signedJWT.getJWTClaimsSet().getAudience());
        } catch (java.text.ParseException e){
            log.error("failed to parse token");
            return false;
        } catch (RuntimeException e){
            log.error("invalid token");
            return false;
        }
    }

    private boolean validateSignature(SignedJWT signedJWT) {

        try {
            return signedJWT.verify(this.verifier);
        } catch (JOSEException e) {
            log.error("invalid signature");
            return false;
        }
    }

    private boolean validateIssuer(String issuer){
        return this.VALID_ISSUER.equals(issuer);
    }

    private boolean validateExpiresAt(Date expirationTime){
        if(expirationTime == null){
            log.error("missing expiration time, invalid token");
            return false;
        }
        if(System.currentTimeMillis() > expirationTime.getTime()){
            log.error("token expired");
            return false;
        }
        return true;
    }

    private boolean validateAudience(List<String> audiences){
        if(audiences == null){
            log.error("missing audience field, invalid token");
            return false;
        }
        for( String audience : audiences) {
            if(audience.equals(CLIEND_ID)){
                log.info("Valid Audience Field");
                return true;
            }
        }
        log.error("invalid audiences value");
        return false;
    }
}
