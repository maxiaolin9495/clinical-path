package com.clinicalpath.security;

import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Base64;
import java.util.Collections;
import java.util.Date;

import javax.annotation.PostConstruct;

import com.clinicalpath.dataObjects.LoginResponse;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.JWSHeader;
import com.nimbusds.jose.JWSSigner;
import com.nimbusds.jose.crypto.RSASSASigner;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;
import com.clinicalpath.dataObjects.User;
import lombok.Getter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class TokenGenerationService {

    @Getter
    @Value("${public_key}")
    private String BACKEND_PUBLIC_KEY;

    @Value("${private_key}")
    private String BACKEND_PRIVATE_KEY;

    @Getter
    @Value("${token.issuer}")
    private String issuer;

    private JWSSigner jwsSigner;

    private static final Logger log = LoggerFactory.getLogger(TokenGenerationService.class);

    @PostConstruct
    private void init() throws NoSuchAlgorithmException, InvalidKeySpecException {
        byte[] clientPrivateKey = Base64.getDecoder().decode(BACKEND_PRIVATE_KEY);
        this.jwsSigner = new RSASSASigner(KeyFactory.getInstance("RSA")
            .generatePrivate(new PKCS8EncodedKeySpec(clientPrivateKey)));

        log.info("initialized jws signer");
    }

    public LoginResponse generateToken(User user) throws Exception {
        log.info("token requested for user " + user.getEmail());
        return buildToken(user);
    }

    private LoginResponse buildToken(User user) throws Exception{
        //set issuedTime and expiredTime, normally the token is valid in 24 hours
        long nowMillis = System.currentTimeMillis();
        Date now = new Date(nowMillis);
        Date exp = new Date(nowMillis + 1000*3600*24);

        //add necessary infos in Payload
        JWTClaimsSet claimsSet = new JWTClaimsSet.Builder()
            .subject(user.getEmail())
            .issuer(issuer)
            .audience(Collections.singletonList(issuer))
            .issueTime(now)
            .expirationTime(exp)
            .build();

        //generate signature for token
        SignedJWT signedJWT =  new SignedJWT(new JWSHeader(JWSAlgorithm.RS512), claimsSet);
        signedJWT.sign(this.jwsSigner);

        log.info("new token signed for user " + user.getEmail());

        return new LoginResponse(signedJWT.serialize());
    }
}
