package com.clinicalpath.security;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.clinicalpath.dataObjects.User;
import com.clinicalpath.repositories.UserDao;
import com.clinicalpath.utils.JWTUtils;
import com.nimbusds.jwt.SignedJWT;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

public class TokenValidationFilter extends BasicAuthenticationFilter {

    private static final Logger log = LoggerFactory.getLogger(TokenValidationFilter.class);

    private final AbstractTokenValidationService tokenValidationService;
    private final AuthenticationEntryPoint authenticationEntryPoint;
    private final UserDao userRepository;

    public TokenValidationFilter(AuthenticationManager authenticationManager,
                                 AuthenticationEntryPoint authenticationEntryPoint,
                                 AbstractTokenValidationService tokenValidationService,
                                 UserDao userRepository) {
        super(authenticationManager);
        this.authenticationEntryPoint = authenticationEntryPoint;
        this.tokenValidationService = tokenValidationService;
        this.userRepository = userRepository;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest req,
                                    HttpServletResponse res,
                                    FilterChain chain) throws IOException, ServletException {

        try {
            Authentication authentication = getAuthentication(req);
            //store the token in the context session
            SecurityContextHolder.getContext()
                    .setAuthentication(authentication);
            chain.doFilter(req, res);
        } catch (UnauthorizedException e) {
            this.authenticationEntryPoint.commence(req, res, new AuthenticationServiceException("", e));
        } catch (Exception e) {
            e.printStackTrace();
            res.sendError(500, "Internal Server Error!");
        }


    }

    // Reads the JWT from the Authorization header, and then uses JWT to validate the token
    private Authentication getAuthentication(HttpServletRequest request) throws UnauthorizedException {
        String header = request.getHeader(HttpHeaders.AUTHORIZATION);
        if (header == null)
            throw new UnauthorizedException("Authorization header empty");

        String token = header.replace(JWTUtils.TOKEN_PREFIX, "");
        SignedJWT jwt;
        try {
            jwt = SignedJWT.parse(token);

            if (tokenValidationService.validateToken(jwt)) {
                User user = this.userRepository
                        .findUserByEmail(
                                jwt.getJWTClaimsSet()
                                        .getSubject()
                        );

                if (user == null)
                    throw new UnauthorizedException("User not found. Maybe the account was deactivated/deleted");

                return new UsernamePasswordAuthenticationToken(user, null, new ArrayList<>());
            }
        } catch (ParseException e) {
            throw new UnauthorizedException("Invalid token");
        }

        log.error("Invalid token");
        throw new UnauthorizedException("Invalid token");
    }
}
