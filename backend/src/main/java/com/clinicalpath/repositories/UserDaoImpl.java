package com.clinicalpath.repositories;

import javax.annotation.Resource;

import com.clinicalpath.dataObjects.User;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;

@Component
public class UserDaoImpl implements UserDao{

    @Resource
    private MongoTemplate mongoTemplate;

    @Override
    public User save(User user)
    {
        return mongoTemplate.save(user);
    }

    @Override
    public User findUserByEmail(String email)
    {
        Query query = new Query(Criteria.where("email").is(email));
        User user = mongoTemplate.findOne(query, User.class);
        return user;
    }

}
