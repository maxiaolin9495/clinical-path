package com.clinicalpath.repositories;

import com.clinicalpath.dataObjects.User;

public interface UserDao {

    User save(User user);

    User findUserByEmail(String email);

}
